import 'dart:ui';

import 'package:flutter/material.dart';

Map<int, Color> mobileDotGreen = {
  50: Color.fromRGBO(55, 73, 70, .1),
  100: Color.fromRGBO(55, 73, 70, .2),
  200: Color.fromRGBO(55, 73, 70, .3),
  300: Color.fromRGBO(55, 73, 70, .4),
  400: Color.fromRGBO(55, 73, 70, .5),
  500: Color.fromRGBO(55, 73, 70, .6),
  600: Color.fromRGBO(55, 73, 70, .7),
  700: Color.fromRGBO(55, 73, 70, .8),
  800: Color.fromRGBO(55, 73, 70, .9),
  900: Color.fromRGBO(55, 73, 70, 1),
};

MaterialColor colorMDGreen = MaterialColor(0xFF8DBAB3, mobileDotGreen);

const MaterialColor colorMDGreen2 = MaterialColor(0xFF8DBAB3, <int, Color>{
  50: Color(0xFF8DBAB3),
  100: Color(0xFF8DBAB3),
  200: Color(0xFF8DBAB3),
  300: Color(0xFF8DBAB3),
  400: Color(0xFF8DBAB3),
  500: Color(0xFF8DBAB3),
  600: Color(0xFF8DBAB3),
  700: Color(0xFF8DBAB3),
  800: Color(0xFF8DBAB3),
  900: Color(0xFF8DBAB3)
});
