String validateName(String value) {
  if (value.length < 3)
    return 'El nombre debe contener más de 2 caracteres';
  else
    return null;
}

String validateCombo(String value) {
  if (value.isEmpty ?? true) {
    return 'Seleccione un monto de recarga';
  } else {
    return null;
  }
}

String validateMobile(String value) {
  if (value.length != 10)
    return 'El número debe tener 10 dígitos';
  else
    return null;
}
String validateCard(String value) {
  if (value.length != 16)
    return 'Su tarjeta debe tener 16 dígitos';
  else
    return null;
}

String validateNumber(String value) {
  if (value.length <= 0)
    return 'Debe escribir un numero';
  else
    return null;
}
String validateMes(String value) {
  if (value.length < 2)
    return '02 caracteres min';
  else
    return null;
}
String validateCode(String value) {
  if (value.length < 3)
    return '03 caracteres como mínimo';
  else
    return null;
}

String validateConfirm(String value) {
  if (value.length != 10)
    return 'Debe confirmar su número';
  else
    return null;
  // else if(value != value2)
  //   return 'Campos no coinciden';
  // else
}

String validatePass(String value) {
// Indian Mobile number are of 10 digit only
  if (value.length != 8)
    return 'El password debe tener 8 caracteres.';
  else
    return null;
}

String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Escriba un email válido';
  else
    return null;
}
