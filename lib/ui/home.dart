import 'package:flutter/material.dart';
import 'package:tadbuy/ui/drawler.dart';
import 'package:tadbuy/ui/form_recarga.dart';
// import 'package:flutter_splash_screen/flutter_splash_screen.dart';

class Home extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            key: _scaffoldKey,
            drawer: DrawerCliente(),
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(170.0),
                child: AppBar(
                  flexibleSpace: Container(
                      height: 150,
                      padding: EdgeInsets.only(
                          top: 50, left: 30.0, right: 30.0, bottom: 30.0),
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        // image: DecorationImage(
                        //   image: AssetImage("assets/images/Fondo-Amarillo.jpg"),
                        //   fit: BoxFit.cover,
                        // ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          SizedBox(
                            width: 50,
                          ),
                          Image.asset(
                            'assets/images/logo_tad.png',
                            fit: BoxFit.contain,
                            height: 70,
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Container(
                              child: FloatingActionButton(
                                backgroundColor:
                                    Color.fromARGB(255, 230, 36, 28),
                                foregroundColor: Colors.white,
                                elevation: 6,
                                onPressed: () =>
                                    _scaffoldKey.currentState.openDrawer(),
                                child: Icon(Icons.menu),
                              ),
                            ),
                          ),
                        ],
                      )),
                  bottom: TabBar(
                    tabs: [
                      Tab(
                        // icon: Icon(Icons.directions_car),
                        text: 'NUEVA RECARGA',
                      ),
                      Tab(
                        text: 'MIS RECARGAS',
                      ),
                      // Tab(icon: Icon(Icons.directions_bike)),
                    ],
                  ),
                  automaticallyImplyLeading: false,
                  // title: Text('Tabs Demo'),
                  backgroundColor: Color.fromARGB(255, 1, 73, 147),
                )),
            body: TabBarView(
              children: [
                Container(
                    padding: EdgeInsets.only(
                        left: 30, right: 30, top: 50, bottom: 50),
                    color: Color.fromARGB(255, 1, 73, 147),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Recarga',
                          style: TextStyle(fontSize: 50, color: Colors.white),
                        ),
                        Text(
                          'tu Celular',
                          style: TextStyle(fontSize: 50, color: Colors.white),
                        ),
                        Text(
                          'Pulsa el botón para realizar una recarga',
                          style: TextStyle(
                              // fontSize: 50,
                              color: Colors.white),
                        ),
                        SizedBox(
                          height: 60,
                        ),
                        SizedBox(
                            width: double.infinity,
                            child: RaisedButton(
                              padding: EdgeInsets.only(
                                  left: 20.0,
                                  right: 20.0,
                                  top: 15.0,
                                  bottom: 15.0),
                              color: Color.fromARGB(255, 230, 36, 28),
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Recarga()),
                                );
                                // _sendPedido(_idCliente, _mySelection, _idZona, _idAsignacion);
                              },
                              child: Text(
                                'Recargar',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat',
                                ),
                              ),
                            )
                            ),
                      ],
                    )),
                Container(
                    padding: EdgeInsets.only(
                        left: 30, right: 30, top: 50, bottom: 50),
                    color: Color.fromARGB(255, 1, 73, 147),
                    child: Container()
                    // Icon(Icons.directions_transit),
                    )
                // Icon(Icons.directions_bike),
              ],
            ),
          ),
        ));
  }
}
