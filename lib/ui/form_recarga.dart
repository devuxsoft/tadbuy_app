import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:tadbuy/ui/drawler.dart';
// import 'package:tadbuy/ui/resumen_recarga.dart';
import 'package:tadbuy/utils/validate.dart';
import 'package:tadbuy/ui/form_pago.dart';
// import 'dart:convert';

enum ConfirmAction { CANCEL, ACCEPT }

class Recarga extends StatefulWidget {
  @override
  _MyRecargaState createState() => _MyRecargaState();
}

class _MyRecargaState extends State<Recarga> {
  SharedPreferences prefs;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  TextEditingController telController = new TextEditingController();
  TextEditingController telConfirmController = new TextEditingController();

  List _montos = [
    "\$20",
    "\$30",
    "\$50",
    "\$100",
    "\$200",
    "\$300",
    "\$400",
    "\$500",
    "\$1000",
    "\$2000"
  ];
  List _tipos = ["Recarga", "Internet", "Paquete"];
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  List<DropdownMenuItem<String>> _dropDownMenuItemsP;
  String _montoSeleccion = '';
  String _tipoSeleccion = '';

  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
    var tel = await getPreferences('tel');
    var monto = await getPreferences('monto');
    var tipo = await getPreferences('tipo');

    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Resumen'),
          content:  Container(
            height: 180,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Confirma la información de tu recarga.'),
                SizedBox(height: 20.0),
                Text('Número: '+tel,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                  ),
                ),
                Text('Monto: '+monto,
                    style: TextStyle(
       
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                  )
                ),
                Text('Tipo: '+tipo,
                  style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18
                  )
                ),
                SizedBox(height: 30.0)
              ],
            ) 
            ),
          actions: <Widget>[
            FlatButton(
              child: const Text('CANCELAR'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.CANCEL);
              },
            ),
            FlatButton(
              child: const Text('CONFIRMAR'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.ACCEPT);
                // print('Direccion:' + _idDir);
                Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Pago()),
                    );
                // _deleteDir();
              },
            )
          ],
        );
      },
    );
  }

  static Future<bool> savePreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  // static Future<bool> savePreferencesBool(String key, bool value) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.setBool(key, value);
  //   return true;
  // }

  static Future<String> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<http.Response> getSWData(String url, {Map bodySend}) async {
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {"Content-type": "application/x-www-form-urlencoded"},
          body: bodySend);
    } catch (e) {
      return http.Response('{"message":"Network Error"}', 500);
    }
    return response;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String monto in _montos) {
      items.add(new DropdownMenuItem(value: monto, child: new Text(monto)));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsP() {
    List<DropdownMenuItem<String>> items = new List();
    for (String tipos in _tipos) {
      items.add(new DropdownMenuItem(value: tipos, child: new Text(tipos)));
    }
    return items;
  }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() async{
    if (_formKey.currentState.validate()) {
      // print('ready to send');

      if(telController.text == telConfirmController.text){
        var tel = telController.text; 
      var monto = _montoSeleccion.toString();
      var tipo = _tipoSeleccion.toString();

      await savePreferences('tel', tel);
      await savePreferences('monto', monto);
      await savePreferences('tipo', tipo);

      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => RecargaResumen()),
      // );
      _asyncConfirmDialog(context);
      }else{
        _showDialog('Ups!', 'El número no coincide');
        setState(() {
          _autoValidate = true;
        });
      }
      
    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _dropDownMenuItemsP = getDropDownMenuItemsP();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          key: _scaffoldKey,
          drawer: DrawerCliente(),
          body: Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 40),
              color: Color.fromARGB(255, 1, 73, 147),
              child: ListView(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Recarga',
                        style: TextStyle(fontSize: 50, color: Colors.white),
                      ),
                      SizedBox(
                        width: 50.0,
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        child: Container(
                          child: FloatingActionButton(
                            backgroundColor: Color.fromARGB(255, 230, 36, 28),
                            foregroundColor: Colors.white,
                            elevation: 6,
                            onPressed: () =>
                                _scaffoldKey.currentState.openDrawer(),
                            child: Icon(Icons.menu),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Llena los campos con tu información para realizar una recarga.',
                    style: TextStyle(
                        // fontSize: 50,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    elevation: 3.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    child: Container(
                      padding:
                          EdgeInsets.only(top: 15.0, left: 20.0, right: 20.0, bottom: 15.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Form(
                        key: _formKey,
                        autovalidate: _autoValidate,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              validator: validateMobile,
                              controller: telController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'Número de Teléfono',
                                  labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color.fromARGB(
                                              255, 254, 153, 2)))),
                            ),
                            TextFormField(
                              validator: validateConfirm,
                              controller: telConfirmController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'Confirmar Número',
                                  labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color.fromARGB(
                                              255, 254, 153, 2)))),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            DropdownButtonFormField<String>(
                              isExpanded: false,
                              hint: Text('Seleccione un monto'),
                              // decoration: InputDecoration.collapsed(hintText: ''),
                              icon: Icon(Icons.keyboard_arrow_down),
                              iconSize: 24,
                              elevation: 0,
                              style: TextStyle(color: Colors.black),
                              onChanged: (newVal) {
                                setState(() {
                                  _montoSeleccion = newVal;
                                });
                              },
                              items: _dropDownMenuItems,
                              value: _montoSeleccion == ''
                                  ? null
                                  : _montoSeleccion,
                              validator: (String value) {
                                if (value?.isEmpty ?? true) {
                                  return 'Seleccione un monto de recarga';
                                } else {
                                  return null;
                                }
                              },
                            ),
                            DropdownButtonFormField<String>(
                              isExpanded: true,
                              hint: Text('Seleccione un tipo'),
                              // decoration: InputDecoration.collapsed(hintText: ''),
                              icon: Icon(Icons.keyboard_arrow_down),
                              iconSize: 24,
                              elevation: 0,
                              style: TextStyle(color: Colors.black),
                              onChanged: (newVal) {
                                setState(() {
                                  _tipoSeleccion = newVal;
                                });
                              },
                              value:
                                  _tipoSeleccion == '' ? null : _tipoSeleccion,
                              items: _dropDownMenuItemsP,
                              validator: (String value) {
                                if (value?.isEmpty ?? true) {
                                  return 'Seleccione un tipo de recarga';
                                } else {
                                  return null;
                                }
                              },
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                        color: Color.fromARGB(255, 230, 36, 28),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                        onPressed: () {
                          _validateInputs();
                          // _sendPedido(_idCliente, _mySelection, _idZona, _idAsignacion);
                        },
                        child: Text(
                          'Recargar',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      launch('http://www.tadbuy.com');
                    },
                    child: Image.asset('assets/images/publicidad.jpg'),
                  ),
                ],
              )),
        ));
  }
}
