// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:tadbuy/ui/home.dart';
import 'package:tadbuy/ui/form_recarga.dart';
// import 'inicio_cliente.dart';
// import 'direcciones.dart';
// import 'micuenta.dart';

class DrawerCliente extends StatefulWidget {
  @override
  _MyDrawer createState() => new _MyDrawer();
}

class _MyDrawer extends State<DrawerCliente> {
  // void openLocationSetting() async {
  //   final AndroidIntent intent = new AndroidIntent(
  //     action: 'android.settings.LOCATION_SOURCE_SETTINGS',
  //   );
  //   await intent.launch();
  // }

  // void logoutUser() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs?.clear();
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => InicioCliente()),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 20.0),
            color: Color.fromARGB(255, 1, 73, 147),
            child: DrawerHeader(
              child: Text(
                'Menú',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20.0),
            color: Color.fromARGB(255, 1, 73, 147),
            height: screenHeight,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    'Nueva Recarga',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Recarga()),
                    );
                  },
                ),
                ListTile(
                  title: Text(
                    'Mis Recargas',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                     Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                // ListTile(
                //   title: Text(
                //     'Direcciones',
                //     style: TextStyle(
                //       color: Colors.white,
                //       fontSize: 16.0,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                //   onTap: () {
                //     // Update the state of the app.
                //     // ...
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //           builder: (context) => Direcciones()),
                //     );
                //   },
                // ),
                // ListTile(
                //   title: Text(
                //     'Cerrar sesión',
                //     style: TextStyle(
                //       color: Colors.white,
                //       fontSize: 16.0,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                //   onTap: () {
                //     // logoutUser();
                //   },
                // ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
