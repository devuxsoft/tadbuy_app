import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';

// import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:tadbuy/ui/drawler.dart';
import 'package:tadbuy/ui/exito.dart';
import 'package:tadbuy/utils/validate.dart';
import 'package:flutter/services.dart';

// import 'dart:convert';

enum ConfirmAction { CANCEL, ACCEPT }

class Pago extends StatefulWidget {
  @override
  _MyPagoState createState() => _MyPagoState();
}

class _MyPagoState extends State<Pago> {
  final String url ="https://tadbuy.devux.com.mx/index.php/Services/payRecharge";
  SharedPreferences prefs;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  TextEditingController ncard = new TextEditingController();
  TextEditingController mes = new TextEditingController();
  TextEditingController ano = new TextEditingController();
  TextEditingController cds = new TextEditingController();
  

  

  
  // prueba de methodos
  // static const platform = const MethodChannel('samples.flutter.dev/battery');

  // Get battery level.
  // String _gatewayPayment = 'Unknown gatewayPayment.';

  // Future<void> _getGatewayPayment({Map bodySend}) async {
  //   String gatewayPayment;
  //   try {
  //     final int result = await platform.invokeMethod('initializeGateway',bodySend);
  //     gatewayPayment = 'initializeGateway  $result % .';
  //   } on PlatformException catch (e) {
  //     gatewayPayment = "Failed to get gatewayPayment: '${e.message}'.";
  //   }

  //   setState(() {
  //     _gatewayPayment = gatewayPayment;
  //   });
  // }

  // List _tipos = ["Recarga", "Internet", "Paquete"];
  // List<DropdownMenuItem<String>> _dropDownMenuItems;
  // List<DropdownMenuItem<String>> _dropDownMenuItemsP;
  // String _montoSeleccion = '';
  // String _tipoSeleccion = '';

  

  

  static Future<bool> savePreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  // static Future<bool> savePreferencesBool(String key, bool value) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.setBool(key, value);
  //   return true;
  // }

  // static Future<String> getPreferences(String key) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return prefs.getString(key);
  // }

  Future<http.Response> getSWData(String url, {Map bodySend}) async {
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {"Content-type": "application/x-www-form-urlencoded"},
          body: bodySend);
    } catch (e) {
      return http.Response('{"message":"Network Error"}', 500);
    }
    return response;
  }

  // List<DropdownMenuItem<String>> getDropDownMenuItems() {
  //   List<DropdownMenuItem<String>> items = new List();
  //   for (String monto in _montos) {
  //     items.add(new DropdownMenuItem(value: monto, child: new Text(monto)));
  //   }
  //   return items;
  // }

  // List<DropdownMenuItem<String>> getDropDownMenuItemsP() {
  //   List<DropdownMenuItem<String>> items = new List();
  //   for (String tipos in _tipos) {
  //     items.add(new DropdownMenuItem(value: tipos, child: new Text(tipos)));
  //   }
  //   return items;
  // }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
      // print('ready to send');

      await savePreferences('ncard', ncard.text);
      await savePreferences('mes', mes.text);
      await savePreferences('ano', ano.text);
      await savePreferences('cds', cds.text);

      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => RecargaResumen()),
      // );
      // _asyncConfirmDialog(context);
      // }else{
      //   _showDialog('Ups!', 'El número no coincide');
      //   setState(() {
      //     _autoValidate = true;
      //   });
      // }
      _enviarDatos();

    } else {
//    If all data are not valid then start auto validation.
      _showDialog('Ups!', 'Debe completar el formulario correctamente');
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void _enviarDatos() async {
    // revisamos campos vacíos
    if (ncard.text != "" &&
        mes.text != "" &&
        ano.text != "" &&
        cds.text != "") {
      // preparamos las variables a enviar en el body
      // var bodySend = {
      //   "ncard": ncard.text,
      //   "mes": mes.text,
      //   "ano": ano.text,
      //   "cds": cds.text,
      //   "movil": await getPreferences('tel'),
      //   "monto": await getPreferences('monto'),
      //   "tipo": await getPreferences('tipo'),
      // };

      await savePreferences('ncard', ncard.text);
      await savePreferences('mes', mes.text);
      await savePreferences('ano', ano.text);
      await savePreferences('cds', cds.text);

      // enviamos y nos traemos la respuesta
      // var respuesta = await this.getSWData(url, bodySend: bodySend);
      // var resBody = json.decode(respuesta.body);
      // var data = resBody;

      // print(bodySend);

      // await savePreferences("code", data[0]['code']);
      // await savePreferences("message", data[0]['message']);
      // var code = await getPreferences("code");
      // var message = await getPreferences("message");

      // _getGatewayPayment(bodySend: bodySend);

      var code = '200';

      if (code == '500') {
        // _showDialog('Aviso', _gatewayPayment);
        // _showDialog('Aviso', message);
      } else {
         _showDialog('Correcto','Su recarga se ha realizado con éxito');
          Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Exito()),
                    );
        // await savePreferencesBool('isLoggedIn', true);
        // await savePreferences("nombre", nombreController.text);
        // await savePreferences("email", emailController.text);
        // await savePreferences("telefono", telController.text);
        // await savePreferences("password", passwordController.text);
        // await savePreferences("id_cliente", _idCliente);

        // _showDialog('Ok', '¡Se han guardado los cambios!');
      }
    } else {
      _showDialog('Ups!', 'Debe llenar todos los campos');
    }
  }

  @override
  void initState() {
    // _dropDownMenuItems = getDropDownMenuItems();
    // _dropDownMenuItemsP = getDropDownMenuItemsP();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          key: _scaffoldKey,
          drawer: DrawerCliente(),
          body: Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 20),
              color: Color.fromARGB(255, 1, 73, 147),
              child: ListView(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Center(
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //     children: [
                  //       RaisedButton(
                  //         child: Text('Try payment'),
                  //         onPressed: _getGatewayPayment,
                  //       ),
                  //       Text(_batteryLevel),
                  //     ],
                  //   ),
                  // ),
                  Row(
                    children: <Widget>[
                      Text(
                        'Pagar',
                        style: TextStyle(fontSize: 50, color: Colors.white),
                      ),
                      SizedBox(
                        width: 100.0,
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        child: Container(
                          child: FloatingActionButton(
                            backgroundColor: Color.fromARGB(255, 230, 36, 28),
                            foregroundColor: Colors.white,
                            elevation: 6,
                            onPressed: () =>
                                _scaffoldKey.currentState.openDrawer(),
                            child: Icon(Icons.menu),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Ingresa la información de tu tarjeta para realizar tu recarga',
                    style: TextStyle(
                        // fontSize: 50,
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    elevation: 3.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    child: Container(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Form(
                        key: _formKey,
                        autovalidate: _autoValidate,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              validator: validateCard,
                              maxLength: 16,
                              controller: ncard,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'Número de Tarjeta',
                                  labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color.fromARGB(
                                              255, 254, 153, 2)))),
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                    width: 120,
                                    margin: EdgeInsets.only(right: 10),
                                    child: TextFormField(
                                      maxLength: 2,
                                      validator: validateMes,
                                      controller: mes,
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          labelText: 'Vence Mes',
                                          labelStyle: TextStyle(
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Color.fromARGB(
                                                      255, 254, 153, 2)))),
                                    )),
                                Container(
                                  width: 120,
                                  child: TextFormField(
                                    validator: validateMes,
                                    controller: ano,
                                    maxLength: 2,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: 'Vence Año',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color.fromARGB(
                                                    255, 254, 153, 2)))),
                                  ),
                                )
                              ],
                            ),
                            
                                 TextFormField(
                                    validator: validateCode,
                                    controller: cds,
                                    maxLength: 3,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: 'Codigo de seguridad',
                                        labelStyle: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey),
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Color.fromARGB(
                                                    255, 254, 153, 2)))),
                                  ),
                            SizedBox(
                              height: 30.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                        color: Color.fromARGB(255, 230, 36, 28),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                        onPressed: () {
                          _validateInputs();
                          // _sendPedido(_idCliente, _mySelection, _idZona, _idAsignacion);
                        },
                        child: Text(
                          'Recargar',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     launch('http://www.tadbuy.com');
                  //   },
                  //   child: Image.asset('assets/images/publicidad.jpg'),
                  // ),
                ],
              )),
        ));
  }
}
