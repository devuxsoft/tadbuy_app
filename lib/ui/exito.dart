import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';


import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tadbuy/ui/drawler.dart';
import 'package:tadbuy/ui/home.dart';
// import 'package:flutter/services.dart';

// import 'dart:convert';

enum ConfirmAction { CANCEL, ACCEPT }

class Exito extends StatefulWidget {
  @override
  _MyExito createState() => _MyExito();
}

class _MyExito extends State<Exito> {
  final String url ="https://tadbuy.devux.com.mx/index.php/Services/saveRecharge";
  List data;
  SharedPreferences prefs;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController ncard = new TextEditingController();
  TextEditingController mes = new TextEditingController();
  TextEditingController ano = new TextEditingController();
  TextEditingController cds = new TextEditingController();
  

  

  
  // prueba de methodos
  // static const platform = const MethodChannel('samples.flutter.dev/battery');
  

  static Future<bool> savePreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  // static Future<bool> savePreferencesBool(String key, bool value) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.setBool(key, value);
  //   return true;
  // }

  static Future<String> getPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<http.Response> getSWData(String url, {Map bodySend}) async {
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: {"Content-type": "application/x-www-form-urlencoded"},
          body: bodySend);
    } catch (e) {
      return http.Response('{"message":"Network Error"}', 500);
    }
    return response;
  }

  // List<DropdownMenuItem<String>> getDropDownMenuItems() {
  //   List<DropdownMenuItem<String>> items = new List();
  //   for (String monto in _montos) {
  //     items.add(new DropdownMenuItem(value: monto, child: new Text(monto)));
  //   }
  //   return items;
  // }

  // List<DropdownMenuItem<String>> getDropDownMenuItemsP() {
  //   List<DropdownMenuItem<String>> items = new List();
  //   for (String tipos in _tipos) {
  //     items.add(new DropdownMenuItem(value: tipos, child: new Text(tipos)));
  //   }
  //   return items;
  // }

  void _showDialog(String titulo, String mensaje) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(titulo),
          content: new Text(mensaje),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  

 void _enviarDatos() async {

      var bodySend = {
        "ncard": await getPreferences('ncard'),
        "mes": await getPreferences('mes'),
        "ano": await getPreferences('ano'),
        "cds": await getPreferences('cds'),
        "movil": await getPreferences('tel'),
        "monto": await getPreferences('monto'),
        "tipo": await getPreferences('tipo')
      };

      print(bodySend);

      var respuesta = await this.getSWData(url, bodySend: bodySend);
      var resBody = json.decode(respuesta.body);
      data = resBody;

      await savePreferences("code", data[0]['code']);
      await savePreferences("message", data[0]['message']);
      var code = await getPreferences("code");
      var message = await getPreferences("message");

      if (code == '500') {
        _showDialog('Error', message);
      } else {
        _showDialog('Correcto', message);
      }
  }

  @override
  void initState() {
    // _dropDownMenuItems = getDropDownMenuItems();
    // _dropDownMenuItemsP = getDropDownMenuItemsP();
    _enviarDatos();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          key: _scaffoldKey,
          drawer: DrawerCliente(),
          body: Container(
              padding: EdgeInsets.only(left: 30, right: 30, top: 20),
              color: Color.fromARGB(255, 1, 73, 147),
              child: ListView(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Center(
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //     children: [
                  //       RaisedButton(
                  //         child: Text('Try payment'),
                  //         onPressed: _getGatewayPayment,
                  //       ),
                  //       Text(_batteryLevel),
                  //     ],
                  //   ),
                  // ),
         
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 100,
                        ),
                        RawMaterialButton(
                          onPressed: () {},
                          elevation: 2.0,
                          fillColor: Colors.white,
                          child: Icon(
                            Icons.check,
                            size: 50.0,
                            color: Colors.green,
                          ),
                          padding: EdgeInsets.all(15.0),
                          shape: CircleBorder(),
                        ),
                        SizedBox(height: 20),
                        Text(
                          'Felicidades',
                          style: TextStyle(fontSize: 50, color: Colors.white),
                        ),
                        Text(
                          'Tu recarga esta lista',
                          style: TextStyle(
                              // fontSize: 50,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                
                 
                  SizedBox(height: 100),
                  SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 15.0, bottom: 15.0),
                        color: Color.fromARGB(255, 230, 36, 28),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                        onPressed: () {
                          // _validateInputs();
                          // _sendPedido(_idCliente, _mySelection, _idZona, _idAsignacion);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Home()),
                          );
                        },
                        child: Text(
                          'Regresa para realizar otra recarga',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 30,
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     launch('http://www.tadbuy.com');
                  //   },
                  //   child: Image.asset('assets/images/publicidad.jpg'),
                  // ),
                ],
              )),
        ));
  }
}
