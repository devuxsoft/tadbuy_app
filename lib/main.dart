// import 'package:donchonito_app/src/providers/push_notifications_provider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:tadbuy/ui/splash-screen.dart';

import 'ui/login.dart';
import 'ui/form_recarga.dart';



void main() => runApp(MyApp());

class MyApp extends StatefulWidget{
  @override
  _MyAppState createState()=>_MyAppState();
}

class _MyAppState extends State<MyApp> {
  SharedPreferences prefs;
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  static Future<bool> savePreferencesBool(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
    return true;
  }

  static Future<bool> savePreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    return true;
  }

  static Future<bool> delPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var regreso = prefs.remove(key);
    return regreso;
  }

  // void navigateUser() async{
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   var status = prefs.getBool('isLoggedIn') ?? false;
  //   // print(status);
  //   if (status) {
  //     Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => InicioCliente()),
  //   );
  //   }
  // }

  @override
  void initState(){
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tadbuy',
      navigatorKey: navigatorKey,
      initialRoute: 'splash',
      routes: {
        'login':(BuildContext context) => Login(),
        'splash':(BuildContext context) => SplashScreen(),
        'form_recarga':(BuildContext context) => Recarga(),
        // 'registro':(BuildContext context) => Register(),
        // 'inicioCliente':(BuildContext context) => InicioCliente(),
        // 'felicidadesE':(BuildContext context) => FelicidadesE(),
      },
      // localizationsDelegates: const [
      //   location_picker.S.delegate,
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      //   GlobalCupertinoLocalizations.delegate,
      // ],
      // supportedLocales: const <Locale>[
      //   Locale('es', 'MX'),
      //   Locale('en', 'US'),
      //   // Locale('ar', ''),
      // ],
      // home: HomePage(),
    );
  }
}






